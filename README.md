We are creating a decentralised YouTube alternative, hosting video files on Sia. We will develop functionality for listing videos, tracking views, built-in advertising, and censorship through an Ethereum smart contract. We are considering incentive models that can drive good behaviour among video uploaders and viewers, but first must define 'good'.

# Project milestones:

- Define a protocol for storing video information on Sia
- Write a webapp for
  - Uploading files conforming with our protocol
  - Retrieving and rendering videos given a skynet link
- Write a Solidity smart contract which can
  - Accept new skynet links to list videos
  - Present the list of listed videos
- Extend this Solidity smart contract to
  - Track views
  - Allow a video uploader to claim authorship
  - Allow patrons to pay an uploader a flat fee
  - Allow sponsors to pay an uploader per view

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
