# v0.0.0 : Bring your own Sia hash for a video upload
- The next iteration will advance this to leverage an Ethereum smart contract for simple, uncensored, un-metadata-ed, unmonetised video listing, rendering this list to a viewer
- The iteration after that will track metadata like views and categories, and render the videos accordingly
- The iteration after that will leverage the 'secret' field of the JSON to let users monetise videos
- The iteration after that will build incentive mechanisms for censorship, according to some parameters for what should and should not be censored

## Uploading
1. Video file is uploaded through the client front-end (simple input type=file)
2. The client takes the video, atob()s it, and crafts the following JSON to put on Sia:

```
{
    "secret" : "", // unused for now, but will be used to let uploaders claim ads
    "format" : ".mov || .mp4 || .ogg",
    "video" : (the atob()ed video bytes)
}
```

3. The client gets the hash of the uploaded JSON and sends this to our backend
4. Our backend goes to the skynet for the above json given the hash, takes the .video field of that JSON, btoa()s it to get the original video bytes, and uploads the original video directly on skynet
5. Our backend maps the sia hash for the JSON to the sia hash for the uploaded video

## Viewing
1. A client requests to view a video by providing its upload JSON skynet hash
2. Our back end takes that hash, and sends back the .secret & .format of that JSON
3. Our back end next looks up what video hash that JSON upload hash maps to, and sends the skynet link for the video to the client
4. The client renders a <video> tag embedding that skynet video, and renders the metadata alongside it, perhaps performing logic with it
