const skynet = require('@nebulous/skynet');
const fs = require('fs');

/*
    A Video is a:
    {
        videoBytes: <string>,
        videoFileType: "mp4" | "mov"
        secretKey: <string>
    }
*/

// String -> Video
async function downloadVideo(link) {
    let filePath = `./${link}.json`;
    await skynet.DownloadFile(
        filePath,
        link,
        skynet.DefaultDownloadOptions
    );
    let downloadedJson = JSON.parse(fs.readFileSync(filePath));
    console.log(`DOWNLOADED JSON:\n${JSON.stringify(downloadedJson, null, 2)}`);
    return downloadedJson;
}

@TODO
async function uploadVideo(link) {
    const skylink = await skynet.UploadFile(
		"./src.jpg",
		skynet.DefaultUploadOptions
	);
	console.log(`Upload successful, skylink: ${skylink}`);
}


downloadVideo();
